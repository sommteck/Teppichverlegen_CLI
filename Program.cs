using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teppichverlegen_CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            // lokale Variablen
            string eingabe_laenge, eingabe_breite;
            double breite, laenge, flaeche, nettoteppich, nettoverlegen, gesamtpreis;

            // Eingabe
            // Benutzerführung
            // Benutzereingabe Länge
            Console.Write("Raumlänge: ");
            eingabe_laenge = Console.ReadLine();
            laenge = Convert.ToDouble(eingabe_laenge);
            // Benutzereingabe Breite
            Console.Write("Raumbreite: ");
            eingabe_breite = Console.ReadLine();
            breite = Convert.ToDouble(eingabe_breite);

            // Berechnung der Fläche
            flaeche = breite * laenge;

            // Berechnung Nettopreis des Teppichs bei Quadratmeterpreis von 35,--€
            nettoteppich = flaeche * 35;

            // Berechnung Nettoverlegen des Teppichs bei Quadratmeterpreis von 22,--€
            nettoverlegen = flaeche * 22;

            // Berechnung des Gesamtpreis inkl. Umsatzsteuer
            gesamtpreis = (nettoteppich + nettoverlegen) * 1.19;

            // Bildschirm löschen
            Console.Clear();

            // Ausgabe
            // Länge
            Console.Write("Raumlänge: ");
            // Ergebnis mit zwei Dezimalstellen
            Console.WriteLine(eingabe_laenge);
            // Breite
            Console.Write("Raumbreite: ");
            // Ergebnis mit zwei Dezimalstellen
            Console.WriteLine(eingabe_breite);
            // Fläche
            Console.Write("Gesamtfläche: ");
            // Ergebnis mit drei Dezimalstellen
            Console.WriteLine(flaeche.ToString("F3"));
            // Nettopreis des Teppichs in Euro
            Console.Write("Nettopreis Teppich in Euro: ");
            // Ergebnis mit zwei Dezimalstellen
            Console.WriteLine(nettoteppich.ToString("F2"));
            // Nettopreis des Verlegens in Euro
            Console.Write("Nettopreis Verlegen in Euro: ");
            // Ergebnis mit zwei Dezimalstellen
            Console.WriteLine(nettoverlegen.ToString("F2"));
            // Gesamtpreis inklusive Umsatzsteuer
            Console.Write("Gesamtpreis in Euro inkl. USt: ");
            // Ergebnis mit zwei Dezimalstellen
            Console.WriteLine(gesamtpreis.ToString("F2"));

            // Programmausgabe stoppen
            Console.Write("... Weiter mit beliebiger Taste.");
            Console.ReadKey();
        }
    }
}